﻿-- Vamos a crear las tablas para la B.D. de una biblioteca con una cardinalidad 1,N
DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE  b20190605;
USE b20190605

-- Creamos la tabla ejemplar
  CREATE TABLE ejemplar(
  codejemplar int, 
  PRIMARY KEY (codejemplar)
      );

-- Creamos la tabla socio
  CREATE TABLE socio(
  codesocio int, 
  PRIMARY KEY (codesocio)  
    );
-- Creamos la tabla presta y como en este caso la cardinalidad es de 1,n la clave única es la de socio.

CREATE TABLE presta(
  ejemplar int,
  socio int,
  fecha_ini date,
  fecha_fin date,
  UNIQUE KEY (socio),
  CONSTRAINT fkprestanejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (codejemplar),
  CONSTRAINT fkprestan FOREIGN KEY (socio) REFERENCES socio (codesocio)
    );