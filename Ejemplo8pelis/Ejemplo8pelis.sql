﻿DROP DATABASE IF EXISTS b20190606pelis;
CREATE DATABASE b20190606pelis;
USE b20190606pelis;

-- Vamos a crear las tablas para la B.D. de un videoclub con una cardinalidad N,N

 
  CREATE TABLE socio (
  codsocio int,
  nombre varchar (100),
  PRIMARY KEY (codsocio)
    );

  CREATE TABLE tfnos (
  codsocio int, 
  tfno int (9),
  PRIMARY KEY (codsocio, tfno),
  CONSTRAINT fktienetfno FOREIGN KEY (codsocio) REFERENCES socio (codsocio)
  
    );

  CREATE TABLE emails (
  codsocio int, 
  email varchar (30),
  PRIMARY KEY (codsocio, email),
  CONSTRAINT fktieneemail FOREIGN KEY (codsocio) REFERENCES socio (codsocio)
    
    );

  CREATE TABLE pelis (
  codpeli int,
  titpeli varchar (100),
  PRIMARY KEY (codpeli)    
    );

  CREATE TABLE alquila (
  codsocio int,
  codpeli int,
  PRIMARY KEY (codpeli,codsocio),
  CONSTRAINT fktienecodsocio FOREIGN KEY (codsocio) REFERENCES socio (codsocio),
  CONSTRAINT fktienecodpeli FOREIGN KEY (codpeli) REFERENCES socio (codsocio)
    );

  CREATE TABLE fecha (    
  codsocio int,
  codpeli int,
  fecha date,
  PRIMARY KEY (codpeli,codsocio,fecha),
  CONSTRAINT fkfechaalquila FOREIGN KEY (codpeli,codsocio) REFERENCES alquila (codpeli,codsocio)
    );