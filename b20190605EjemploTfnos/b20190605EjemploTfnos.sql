﻿-- Ejercicio del módulo 1 tema 3 página 2

DROP DATABASE IF EXISTS b20190605ejemplostfnos;
CREATE DATABASE  b20190605ejemplostfnos;
USE b20190605ejemplostfnos;


CREATE OR REPLACE TABLE personas(
  dni varchar(10),
  nombre varchar(50),
  PRIMARY KEY (dni)
  );

 INSERT INTO personas (dni, nombre)
  VALUES 
  ('d1', 'n1'), -- estos valores son las abreviaturas de dni1 y nombre2.
  ('d2', 'n2'), -- estos valores son las abreviaturas de dni2 y nombre2.
  ('d3', 'n3'); -- estos valores son las abreviaturas de dni3 y nombre3.

  
CREATE OR REPLACE TABLE telefonos(
  dni varchar (10),
  num varchar(12),
  PRIMARY KEY(dni,num), -- se pueden repetir por separado pero no a la vez.
  CONSTRAINT fktfnospersonas FOREIGN KEY(dni) REFERENCES personas(dni)
  );

INSERT INTO telefonos (dni, num)
  VALUES
  ('d1', 't1'), -- A la persona con DNI1 le estamos metiendo 2 números de teléfono.
  ('d1'  't2'), -- Este es el número de teléfono 2.
  ('d2'  't3'); -- A esta persona con DNI2 le estamos metiendo el número de teléfono 3.
