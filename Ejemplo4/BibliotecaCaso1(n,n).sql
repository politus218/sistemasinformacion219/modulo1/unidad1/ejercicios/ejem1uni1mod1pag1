﻿DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE  b20190605;
USE b20190605




-- Vamos a crear las tablas para la B.D. de una biblioteca con una cardinalidad N,N

 -- Creamos la tabla ejemplar
  CREATE TABLE ejemplar(
  codejemplar int, 
  PRIMARY KEY (codejemplar)
  
    );

-- Creamos la tabla socio
  CREATE TABLE socio(
  codesocio int, 
  PRIMARY KEY (codesocio)  
    );

-- Creamos la tabla presta

  CREATE TABLE presta(
  ejemplar int,
  socio int,
  fecha_ini date,
  fecha_fin date,
  PRIMARY KEY (ejemplar, socio),
  CONSTRAINT fkprestanejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (codejemplar),
  CONSTRAINT fkprestansocio FOREIGN KEY (socio) REFERENCES socio (codesocio)
    );